#include "search_kyusin.hpp"

enum Pattern{
		INIT,
		CHECK,
		GO_STRAIGHT,
		GO_LEFT,
		GO_RIGHT,
		BACK_TURN,
		GOAL,
		WRITE_MAP
};
//kyuusin
void search_kyusin(Machine &machine, MapMbed &map, BusOut &led,int _speed){

	//uint8_t walls = 0;
	// map.update(walls);
	int pattern = INIT;
	map.make_walkmap();
	while(1){
		switch(pattern){
			case INIT:
				machine.move_d(150, CENTER_BLOCK);
				// machine.move(DEFAULT_SPEED, CENTER_BLOCK);
				machine.motor_stop();
				wait(0.5);
				machine.move_d(_speed, HALF_BLOCK, MODE_SKIP_STOP);
				pattern = CHECK;
				break;

			case CHECK:
				map.update(!machine.is_opened_wall(LEFT_MACHINE),
						       !machine.is_opened_wall(FRONT_MACHINE),
					         !machine.is_opened_wall(RIGHT_MACHINE));
				map.make_walkmap();

				if(map.check_goal()){pattern = GOAL;break;}
				if(map.get_cnt_walkmap()==255){pattern = WRITE_MAP;break;}

				if(machine.is_opened_wall(FRONT_MACHINE) && map.get_cnt_walkmap()-map.get_cnt_walkmap(FRONT_MACHINE)==1 )pattern = GO_STRAIGHT;
				else if(machine.is_opened_wall(LEFT_MACHINE) && map.get_cnt_walkmap()-map.get_cnt_walkmap(LEFT_MACHINE)==1 )pattern = GO_LEFT;
				else if(machine.is_opened_wall(RIGHT_MACHINE) && map.get_cnt_walkmap()-map.get_cnt_walkmap(RIGHT_MACHINE)==1 )pattern = GO_RIGHT;
				else pattern = BACK_TURN;
				break;

			case GO_STRAIGHT:
				// machine.motor_stop();
				// wait(0.2);
				//map.writefile_mapdata();
				map.change_direction(FRONT_MACHINE);
				machine.move_p(_speed, ONE_BLOCK);
				pattern = CHECK;
				break;

			case GO_LEFT:
				// machine.motor_stop();
				// wait(0.2);
				map.change_direction(LEFT_MACHINE);
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_START);
				machine.motor_stop();
				led = 0b0001;
				wait(0.5);
				machine.turn(LEFT_MACHINE);
				machine.motor_stop();
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_STOP);
				pattern = CHECK;
				break;

			case GO_RIGHT:
				// machine.motor_stop();
				// wait(0.2);
				map.change_direction(RIGHT_MACHINE);
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_START);
				machine.motor_stop();
				led = 0b1000;
				wait(0.5);
				machine.turn(RIGHT_MACHINE);
				machine.motor_stop();
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_STOP);
				pattern = CHECK;
				break;

			case BACK_TURN:
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_START);
				map.change_direction(TURN_MACHINE);
				machine.motor_stop();
				wait(0.5);
				machine.turn(TURN_MACHINE);
				machine.motor_stop();
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_STOP);
				pattern = CHECK;
				break;

			case GOAL:
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_START);
				machine.motor_stop();
				wait(0.2);
				// machine.turn(TURN);
				// machine.motor_stop();
				// wait(0.2);
				machine.motor_off();
				return;
				// pattern = WRITE_MAP;
				// break;

			case WRITE_MAP:
				wait(1);
				map.writefile_mapdata();
				wait(1);
				led = 0b11111;
				wait(0.5);
				led = 0b0000;
				return;
		}
	}
}

void search_kyusin2(Machine &machine, MapMbed &map, BusOut &led,int _speed){

	//uint8_t walls = 0;
	// map.update(walls);
	int pattern = INIT;
	map.make_walkmap();
	while(1){
		switch(pattern){
			case INIT:
				machine.move_d(150, CENTER_BLOCK);
				// machine.move(DEFAULT_SPEED, CENTER_BLOCK);
				machine.motor_stop();
				wait(0.5);
				machine.move_d(_speed, HALF_BLOCK, MODE_SKIP_STOP);
				pattern = CHECK;
				break;

			case CHECK:
				map.update(!machine.is_opened_wall(LEFT_MACHINE),
						       !machine.is_opened_wall(FRONT_MACHINE),
					         !machine.is_opened_wall(RIGHT_MACHINE));
				map.make_walkmap();

				if(map.check_goal()){pattern = GOAL;break;}
				if(map.get_cnt_walkmap()==255){pattern = WRITE_MAP;break;}

				if(machine.is_opened_wall(FRONT_MACHINE) && map.get_cnt_walkmap()-map.get_cnt_walkmap(FRONT_MACHINE)==1 )pattern = GO_STRAIGHT;
				else if(machine.is_opened_wall(LEFT_MACHINE) && map.get_cnt_walkmap()-map.get_cnt_walkmap(LEFT_MACHINE)==1 )pattern = GO_LEFT;
				else if(machine.is_opened_wall(RIGHT_MACHINE) && map.get_cnt_walkmap()-map.get_cnt_walkmap(RIGHT_MACHINE)==1 )pattern = GO_RIGHT;
				else pattern = BACK_TURN;
				break;

			case GO_STRAIGHT:
				// machine.motor_stop();
				// wait(0.2);
				//map.writefile_mapdata();
				map.change_direction(FRONT_MACHINE);
				machine.move_p(_speed, ONE_BLOCK);
				pattern = CHECK;
				break;

			case GO_LEFT:
				// machine.motor_stop();
				// wait(0.2);
				map.change_direction(LEFT_MACHINE);
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_START);
				machine.motor_stop();
				led = 0b0001;
				wait(0.5);
				machine.turn(LEFT_MACHINE);
				machine.motor_stop();
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_STOP);
				pattern = CHECK;
				break;

			case GO_RIGHT:
				// machine.motor_stop();
				// wait(0.2);
				map.change_direction(RIGHT_MACHINE);
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_START);
				machine.motor_stop();
				led = 0b1000;
				wait(0.5);
				machine.turn(RIGHT_MACHINE);
				machine.motor_stop();
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_STOP);
				pattern = CHECK;
				break;

			case BACK_TURN:
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_START);
				map.change_direction(TURN_MACHINE);
				machine.motor_stop();
				wait(0.5);
				machine.turn(TURN_MACHINE);
				machine.motor_stop();
				wait(0.5);
				machine.motor_update(-50, -50);
				wait(1.0);
				machine.move(_speed, CENTER_BLOCK);
				machine.motor_stop();
				wait(0.2);
				machine.turn(LEFT_MACHINE);
				machine.motor_stop();
				wait(0.2);
				machine.motor_update(-50, -50);
				wait(1.0);
				machine.move(_speed, CENTER_BLOCK);
				machine.motor_stop();
				wait(0.2);
				machine.turn(RIGHT_MACHINE);
				machine.motor_stop();
				wait(0.2);
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_STOP);
				pattern = CHECK;
				break;

			case GOAL:
				machine.move_d(_speed, ONE_BLOCK/2, MODE_SKIP_START);
				machine.motor_stop();
				wait(0.2);
				// machine.turn(TURN);
				// machine.motor_stop();
				// wait(0.2);
				machine.motor_off();
				return;
				// pattern = WRITE_MAP;
				// break;

			case WRITE_MAP:
				wait(1);
				map.writefile_mapdata();
				wait(1);
				led = 0b11111;
				wait(0.5);
				led = 0b0000;
				return;
		}
	}
}
