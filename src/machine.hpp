#ifndef MACHINE_H
#define MACHINE_H

#include <mbed.h>
#include "mslm/mslm.h"
#include "defines.hpp"


class Machine
{
private:
  StepMotor lm, rm;

  AnalogOut lef;

public:
  DistanceSensor ls, fs, rs, rrs, lls;
  Machine():
  lm(p24, p26, p25, true, p23), rm(p10, p12, p11, false, p9),
  ls(p15), fs(p16), rs(p17), rrs(p19), lls(p20),
  lef(p18)
  {
    lef = 0.07/3.3;
  }

  void motor_update(int lspeed, int rspeed){
    lm.update(lspeed);
    rm.update(rspeed);

    
  }

  void motor_on(){
    lm.Start();
    rm.Start();
    wait(0.5);
  }

  void motor_off(){
    lm.kill();
    rm.kill();
  }

  void motor_stop(){
    lm.update(0);
    rm.update(0);
  }

  void turn(int _direction){
    int lm_speed = (_direction==LEFT_MACHINE)?-TURN_SPEED:TURN_SPEED;
    int rm_speed = (_direction==LEFT_MACHINE)?TURN_SPEED:-TURN_SPEED;
    int pulse = (_direction==TURN_MACHINE)?to_pulse(QUARTER_TURN)*2:to_pulse(QUARTER_TURN);
    lm.reset_cnt();
    rm.reset_cnt();

    while(1){
      if(lm.pulse_cnt() > pulse || rm.pulse_cnt() > pulse)
      break;
      lm.update(lm_speed);
      rm.update(rm_speed);
    }
    lm.update(0);
    rm.update(0);
    lm.reset_cnt();
    rm.reset_cnt();
    wait(0.2);
  }



  void move_test_pulse(int _pulse){
    lm.Start();
    rm.Start();
    rm.reset_cnt();
    lm.reset_cnt();
    while(rm.pulse_cnt() <= _pulse){
      lm.update(TEST_SPEED);
      rm.update(TEST_SPEED);
    }
    lm.update(0);
    rm.update(0);
    rm.reset_cnt();
    lm.reset_cnt();
    wait(0.5);
    lm.kill();
    rm.kill();
  }

  void move_test_mm(int _mm){
    lm.Start();
    rm.Start();
    rm.reset_cnt();
    lm.reset_cnt();
    while(rm.pulse_cnt() <= to_pulse(_mm)){
      lm.update(TEST_SPEED);
      rm.update(TEST_SPEED);
    }
    lm.update(0);
    rm.update(0);
    rm.reset_cnt();
    lm.reset_cnt();
    wait(0.5);
    lm.kill();
    rm.kill();
  }

  bool is_opened_wall(int _direction){
    if(LEFT_MACHINE == _direction && TH_ONE_SIDEWALL < lls.get_val())return true;
    else if(RIGHT_MACHINE == _direction && TH_ONE_SIDEWALL < rrs.get_val())return true;
    else if(FRONT_MACHINE == _direction && TH_FRONTWALL + 80 < fs.get_val())return true;
    else return false;
  }


  inline void p_control(int speed){
    int LS = ls.get_val();
    int RS = rs.get_val();
    int FS = fs.get_val();
    const int sensor_sum = LS + RS;

    if(sensor_sum > TH_SIDEWALL){
      // if(LS > TH_ONE_SIDEWALL+60 && RS < TH_ONE_SIDEWALL+60 ){
      //   LS = TH_SIDEWALL+60 - RS;
      // }else if(RS > TH_ONE_SIDEWALL+60 && LS < TH_ONE_SIDEWALL+60 ){
      //   RS = TH_SIDEWALL+60 - LS;
      // }else{
        LS = RS = 0;
      // }
    }

    if(FS < TH_FRONTWALL-30){
      LS = RS = 0;
    }

    const int sensor_diff = LS - RS;
    // const int sensor_diff =  0;
    lm.update(speed - (sensor_diff * KP));
    rm.update(speed + (sensor_diff * KP));
    wait(0.005);
  }

  void move_p(int _speed, int _mm/*, bool _stop_enable*/){
			lm.reset_cnt();
			rm.reset_cnt();
			while(rm.pulse_cnt() <= to_pulse(_mm)){
				p_control(_speed);

        if(fs.get_val() < 60){
          lm.update(0);
          rm.update(0);
          wait(0.5);
          break;
        }
			}
			/*if(_stop_enable){
				lm.update(0);
				rm.update(0);
			}*/
			lm.reset_cnt();
			rm.reset_cnt();
		}

    #define ACCEL_P 1.5
  	void move_d(int speed,int distance,int _mode = MODE_STOP){
  		lm.reset_cnt();
  		rm.reset_cnt();
  		// pluse_diff = 0;
  		int seq = (_mode==MODE_SKIP_START)?1:0;
  		double motor_pulse = (rm.pulse_cnt() + lm.pulse_cnt())/2.0;
  		double count = to_pulse(distance);
  		const int lowest_speed = 100;
  		double now_speed = lowest_speed; //最低スピード代入
  		double acc_count = (speed - now_speed) / ACCEL_P;  //加速に必要なパルス数
  		double keep_count = count - (acc_count * 2);
  		int keep_speed = 0;

  		if ((acc_count*2) > count) {
  			keep_count = 0;
  			acc_count -= ((acc_count * 2) - count)/2;
  		}
  		//now_speed = speed_ave;
  		while(motor_pulse  <= count)
  		{
  			motor_pulse = (rm.pulse_cnt() + lm.pulse_cnt())/2;
  			if (true)
  			{
  				switch(seq)
  				{
  					case 0:
  					now_speed = lowest_speed + (motor_pulse * ACCEL_P);
  					if(motor_pulse > acc_count){ seq = 1;  break; }
  					break;
  					case 1:
  					// Adjust deceleration distance by sensor value
  					if(motor_pulse > acc_count + keep_count && _mode!=MODE_RUNNING){seq = 2; break; }
            if(fs.get_val() < 130 ){seq = 2; break; }
  					now_speed = speed;
  					break;
  					case 2:
  					now_speed = lowest_speed +(count - motor_pulse)*ACCEL_P;
  					if(now_speed < lowest_speed) now_speed = lowest_speed;
  					break;
  				}
  			}

  			if(keep_speed != now_speed){
  				keep_speed = now_speed;
  				printf("%d\n\r",now_speed);
  			}
  			// if((check_front_left()<=FRONTWALL-10 || check_front_right()<=FRONTWALL-10) &&
  			//    ( motor_pulse >= HALFBLOCK_PULSE ))break;
  			p_control(now_speed); //P制御ありラン
  		}
  		if(_mode==MODE_STOP)motor_stop();
  		lm.reset_cnt();
  		rm.reset_cnt();
  	}


    void move(int _speed, int _mm/*, bool _stop_enable*/){
			lm.reset_cnt();
			rm.reset_cnt();
			while(lm.pulse_cnt() <= to_pulse(_mm)){
				motor_update(_speed, _speed);
        if(fs.get_val() < 55){
          break;
        }
			}
			/*if(_stop_enable){
				lm.update(0);
				rm.update(0);
			}*/
			lm.reset_cnt();
			rm.reset_cnt();
		}


  };

#endif

// DigitalOut m3_r(p9);
// DigitalOut clock_r(p10);
// DigitalOut cwccw_r(p11);
// DigitalOut reset_r(p12);
//
// DigitalOut m3_l(p23);
// DigitalOut clock_l(p24);
// DigitalOut cwccw_l(p25);
// DigitalOut reset_l(p26);
