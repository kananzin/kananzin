#include <mbed.h>
#include "machine.hpp"
#include "defines.hpp"
#include "mslm/sensor.h"
#include "mslm/switch.h"
#include "search_kyusin.hpp"
#include "mslm/map_mbed.h"

DigitalOut m3_r(p9);
DigitalOut clock_r(p10);
DigitalOut cwccw_r(p11);
DigitalOut reset_r(p12);

DigitalOut m3_l(p23);
DigitalOut clock_l(p24);
DigitalOut cwccw_l(p25);
DigitalOut reset_l(p26);

AnalogOut ref(p18);

DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);

BusOut _led(LED1, LED2, LED3, LED4);
//PwmOut f_sens(LED1);

Serial pc(USBTX,USBRX);

Machine machine;
MapMbed map;

// DistanceSensor leftdistancesensor(p15);
// DistanceSensor frontdistancesensor(p16);
Switch sw1(p5, PullUp); //orange_l
Switch sw2(p6, PullUp); //orange_r
Switch sw3(p7, PullUp); //blue
Switch sw4(p8, PullUp); //red

namespace Main
{
  enum Mode
  {
    SelectMode,
    SerialMode,
    kyusinMode300,
    kyusinMode400,
    kyusinMode500,
    kyusinMode250,
    CalibrationMode,
    MoveTest,
    TestTurnMode,
    CheckWallMode,
  };
}

void Calibration(Machine &a){
  int sum = 0;
  int ans;
  _led = 0;
  for(int i = 0; i < 10; i++){
    wait(0.1);
    _led = i;
    sum = sum + (a.ls.get_val()-a.rs.get_val());
  }

  ans = (sum / 10)/2;
  a.ls.set_corecction(-ans);
  a.rs.set_corecction(ans);
}

int main()
{

  int mode = Main::SelectMode;
  int cnt = 0;
  map.set_start(0, 0);
  map.set_goal(8, 8);
  map.set_size(16,16);
  map.init();
  _led =0b1111;
  // pc.baud(115200);
   // PinMode inSet = PullDown;

   while(1)
   {
     switch (mode)
     {
       case Main::SelectMode:
       if(sw1.update()){
         cnt++;
       }else if(sw2.update()){
         cnt--;
       }else if(sw4.update()){
         mode = cnt;
       }
       _led = cnt;
       break;

       case Main::SerialMode:
       _led= 0b0000;
       wait(0.2);
       // pc.printf("\r\b\r");
       pc.printf("LS = %4d || FS = %4d || RS = %4d LLS = %4d || RRS = %4d Sidewall=%d\r\n",
        machine.ls.get_val(),
        machine.fs.get_val(),
        machine.rs.get_val(),
        machine.lls.get_val(),
        machine.rrs.get_val(),
        machine.ls.get_val()+machine.rs.get_val()
      );
       // pc.printf("sums sidewall = %d\r\n", machine.ls.get_val()+machine.rs.get_val());

       if(sw3.update())
       {
         mode = Main::SelectMode;
       }
       break;

       case Main::kyusinMode250:
       Calibration(machine);
       _led = 0b1111;
       wait(0.5);
       _led = 0b0000;

       machine.motor_on();
       search_kyusin2(machine, map, _led, 250);
       machine.motor_stop();
       wait(0.5);
       machine.motor_off();
       map.reset();

       mode = Main::SelectMode;
       break;

       case Main::kyusinMode300:
       Calibration(machine);
       _led = 0b1111;
       wait(0.5);
       _led = 0b0000;

       machine.motor_on();
       search_kyusin(machine, map, _led, 300);
       machine.motor_stop();
       wait(0.5);
       machine.motor_off();
       map.reset();

       mode = Main::SelectMode;
       break;

       case Main::kyusinMode400:
       Calibration(machine);
       _led = 0b1111;
       wait(0.5);
       _led = 0b0000;

       machine.motor_on();
       search_kyusin(machine, map, _led, 400);
       machine.motor_stop();
       wait(0.5);
       machine.motor_off();
       map.reset();

       mode = Main::SelectMode;
       break;

       case Main::kyusinMode500:
       Calibration(machine);
       _led = 0b1111;
       wait(0.5);
       _led = 0b0000;

       machine.motor_on();
       search_kyusin(machine, map, _led, 500);
       machine.motor_stop();
       wait(0.5);
       machine.motor_off();
       map.reset();

       mode = Main::SelectMode;
       break;


       case Main::MoveTest:
       _led = 0b1111;
       wait(0.5);
       _led = 0b0000;
       //machine.move_test_pulse(1000);//405mm
       Calibration(machine);
       machine.motor_on();
       machine.move_d(TEST_SPEED, ONE_BLOCK*7);
       machine.motor_stop();
       wait(0.5);
       machine.motor_off();
       mode = Main::SelectMode;
       break;

       case Main::TestTurnMode:
       _led = 0b1111;
       wait(0.5);
       _led = 0b0000;

       machine.motor_on();
       machine.turn(TURN_MACHINE);
       machine.turn(TURN_MACHINE);
       machine.motor_stop();
       wait(0.5);
       machine.motor_off();

       mode = Main::SelectMode;
       break;

       case Main::CheckWallMode:
       _led = 0b0000;

       led1 = machine.is_opened_wall(RIGHT_MACHINE);
       led2 = machine.is_opened_wall(FRONT_MACHINE);
       led3 = machine.is_opened_wall(FRONT_MACHINE);
       led4 = machine.is_opened_wall(LEFT_MACHINE);
       if(sw3.update()){
         mode = Main::SelectMode;
       }
       break;

       case Main::CalibrationMode:


       break;
     }
   }
 }


//     if(sw1.update())
//     {
//       led1 = 1;
//       wait(0.5);
//       led1 = 0;
//       wait(0.5);
//     }
//     if(sw2.update())
//     {
//       led2 = 1;
//       wait(0.5);
//       led2 = 0;
//       wait(0.5);
//     }
//     if(sw3.update())
//     {
//       led3 = 1;
//       wait(0.5);
//       led3 = 0;
//       wait(0.5);
//     }
//     if(sw4.update())
//     {
//       led4 = 1;
//       wait(0.5);
//       led4 = 0;
//       wait(0.5);
//   }
// }

// distancesensor.left_Sensor(p15);
// distancesensor.front_Sensor(p16);
// right_Sensor(p17);

  /* while (1) {

   }*/

   /*m3_r = 1;
   clock_r = 0;
   cwccw_r = 1;
   reset_r = 0;

   m3_l = 1;
   clock_l = 0;
   cwccw_l =1;
   reset_l = 0;

   ref = 0.03/3.3;

   led1 = 1;
   wait(1);
   led2 = 1;
   wait(1);
   led3 = 1;
   wait(1);
   led4 = 4;
   wait(1);

   m3_r = 0;
   m3_l = 0;

    while(1)
     {
       clock_r = 1;
       clock_l = 1;
       wait (0.01);
       clock_r = 0;
       clock_l = 0;
       wait (0.01);
    }*/
