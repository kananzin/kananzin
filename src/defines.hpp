#ifndef DEFINES_H
#define DEFINES_H

#define ONE_BLOCK     180
#define HALF_BLOCK    90
#define CENTER_BLOCK  45
#define TEST_SPEED   400
#define TURN_SPEED    140
#define TREAD_WIDTH   86.6
#define QUARTER_TURN  (TREAD_WIDTH * 3.1415) / 4.0
#define TH_SIDEWALL			280
#define TH_ONE_SIDEWALL 100
#define TH_FRONTWALL		135
#define SEARCH_SPEED		400

#define MODE_RUNNING 			(0)
#define MODE_SKIP_STOP 		(0)
#define MODE_STOP    			(1)
#define MODE_SKIP_START 	(2)

#define KP 0.8
#define KI 0.01

#define MODE_RUNNING 				(0)
#define MODE_SKIP_STOP 			(0)
#define MODE_STOP    				(1)
#define MODE_SKIP_START 		(2)



inline int to_mm(int _pulse){
	return 1000.0/405.0 * _pulse;
	//return 2000.0/500.0 * _pulse;
}

inline int to_pulse(double _mm){
	// return (double)(2000.0/410.0 * _mm);
	return (double)(1000.0/407.0 * _mm);
}

#endif
